library(data.table)
library(reshape2)
library(dirmult)
library(lattice)

# functions need to prepare data

source("stats.r")

# read data

hcpTimeTable <- readRDS("./data/hcpTableTime_20141230.RData")
hcpTable <- readRDS("./data/hcpTable_20150213.RData")
repTable <- readRDS("./data/repTable_20141218.RData")
interactionTable <- readRDS("./data/interactionTable_20141218.RData")
tacticTable <- readRDS("./data/tacticTable_20141218.RData")

interactionTable <- droplevels(interactionTable[Activity %in% c("RTE Clicked","RTE Opened","Send RTE","Send iRep Letter","Visit")])

############
# setup dates
############
today <- Sys.Date()
start <- as.Date("2014-05-01")

######## experimental section
interactionTable[is.na(Tactic)]$Tactic <- "None"    # may want to change this
interactionTable[,Week:=cut(date,"week")]
interactionTable[,Month:=cut(date,"month")]
setnames(interactionTable,"date","Day")

setkey(repTable,"rep")
setkey(interactionTable,"rep")
interactionTable <- repTable[interactionTable]

setkey(interactionTable,"Tactic")
setkey(tacticTable,"Tactic")
interactionTable <- tacticTable[interactionTable]

hcpTable[,c("N","P","P2"):=NULL]  # cleanup

# restrict time interval
interactionTable <- subset(interactionTable,Day>=start & Day <= today)

# process hcpTimeTable
hcpTimeTable <- subset(hcpTimeTable,as.Date(Month)>=start & as.Date(Month)<=today)
m <- unique(hcpTimeTable$Month)
h <- unique(hcpTimeTable$hcp)
g <- as.data.table(expand.grid(h,m))
setnames(g,1:2,c("hcp","Month"))
setkey(g,hcp,Month)
setkey(hcpTimeTable,hcp,Month)
hcpTimeTable <- hcpTimeTable[g]
hcpTimeTable[is.na(TRx)]$TRx <- 0
hcpTimeTable[is.na(NRx)]$NRx <- 0
hcpTimeTable[,TRx:=round(TRx)]
hcpTimeTable$Month <- as.Date(hcpTimeTable$Month)


# add NP estimates
for(i in unique(hcpTable$hcp))
    {
        hcpTable[hcp==i,c("N","Propensity","Delta_Propensity"):= estimateNP(hcpTimeTable[hcp==i])]
    }
hcpTable[,Mean:=round(N)*Propensity]

# add prob of seeing specific TRx value
for(i in unique(hcpTable$hcp))
    {
        m <- hcpTable[hcp==i]$Mean
        hcpTimeTable[hcp==i,Likelihood:=ifelse(m<TRx,pbinom(TRx,round(hcpTable[hcp==i]$N),hcpTable[hcp==i]$Propensity),0)]
   }

for(i in hcpTable$hcp)
    {
        t <- hcpTimeTable[hcp==i]$Likelihood
        j <- length(t)
        if(j>1)hcpTable[hcp==i,DP4:=mean(t[(j-1):j])-mean(t)]
        if(j>2)hcpTable[hcp==i,DP3:=mean(t[(j-2):j])-mean(t)]
        if(j>3)hcpTable[hcp==i,DP2:=mean(t[(j-3):j])-mean(t)]
    }

# add event counts from interactionTable to the hcpTimeTable
t <- as.data.table(as.data.frame(xtabs(~hcp+Month+Tactic+Activity,interactionTable)))
t <- t[Freq>0]
setkey(t,Month)
treatments <- as.data.table(dcast(t,hcp~Activity+Tactic+Month,value.var="Freq"))
setkey(treatments,hcp)

target <- hcpTimeTable[Month=="2014-09-01",list(hcp,Likelihood)]
setkey(target,hcp)

g <- target[treatments]
g[is.na(g)] <- 0


# fit <- glmnet(as.matrix(g[,3:158,with=FALSE]),g$Likelihood)
cvfit <- cv.glmnet(as.matrix(g[,3:158,with=FALSE]),round(g$Likelihood),family="binomial",alpha=.5)
coef(cvfit,s=.075)


cvfit <- cv.glmnet(as.matrix(g[,3:158,with=FALSE]),g$Likelihood,family="gaussian",alpha=.5,intercept=FALSE)
coef(cvfit,s='lambda.min')
xyplot(predict(cvfit,newx=diag(156),s=.1),g$Likelihood)

reduce <- function(A,dim) {
    #Calculates the SVD
    sing <- svd(A)

    #Approximate each result of SVD with the given dimension
    u<-as.matrix(sing$u[, 1:dim])
    v<-as.matrix(sing$v[, 1:dim])
    d<-as.matrix(diag(sing$d)[1:dim, 1:dim])

    #Create the new approximated matrix
    return(u%*%d%*%t(v))
}
