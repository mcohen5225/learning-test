library(data.table)
library(reshape2)

# functions need to prepare data

source("stats.r")
source("utils.r")

# read data

metaTable <- data.table(table=character(),field=character(),type=character())
hcpTimeTable <- readRDS("./data/hcpTableTime_20141230.RData")
hcpTable <- readRDS("./data/hcpTable_20150213.RData")
repTable <- readRDS("./data/repTable_20141218.RData")
interactionTable <- readRDS("./data/interactionTable_20141218.RData")
tacticTable <- readRDS("./data/tacticTable_20141218.RData")


############
# setup dates
############
today <- Sys.Date()
start <- as.Date("2014-05-01")

# restrict time interval and remove junk
setnames(interactionTable,"date","Day")
interactionTable <- subset(interactionTable,Day>=start & Day <= today)
interactionTable <- interactionTable[Activity!="Suggestion" & Activity!="Sync"]

setkey(repTable,"rep")
setkey(interactionTable,"rep")
interactionTable <- repTable[interactionTable]

setkey(interactionTable,"Tactic")
setkey(tacticTable,"Tactic")
interactionTable <- tacticTable[interactionTable]

######## cleanup
hcpTable[,c("N","P","P2"):=NULL]  # cleanup

interactionTable[is.na(Tactic)]$Tactic <- "None"    # may want to change this
interactionTable[,Week:=cut(Day,"week")]
interactionTable[,Month:=cut(Day,"month")]

interactionTable$Tactic <- gsub("-","",interactionTable$Tactic)
interactionTable$Tactic <- gsub("/","_",interactionTable$Tactic)
interactionTable$Tactic <- gsub(" ","_",interactionTable$Tactic)
interactionTable$Activity <- gsub("-","",interactionTable$Activity)
interactionTable$Activity <- gsub("/","_",interactionTable$Activity)
interactionTable$Activity <- gsub(" ","_",interactionTable$Activity)

interactionTable$Activity <- as.factor(interactionTable$Activity)
interactionTable$Tactic <- as.factor(interactionTable$Tactic)


# add to metaTable
addToMetaTable("interactionTable","Tactic","Group")
addToMetaTable("interactionTable","Message","Group")
addToMetaTable("interactionTable","Name","Group")
addToMetaTable("interactionTable","District","Group")
addToMetaTable("interactionTable","Role","Group")
addToMetaTable("interactionTable","hcp","Group")
addToMetaTable("interactionTable","Activity","Group")
addToMetaTable("interactionTable","Month","Time")
addToMetaTable("interactionTable","Day","Time")
addToMetaTable("interactionTable","Week","Time")

# process hcpTimeTable
hcpTimeTable <- subset(hcpTimeTable,as.Date(Month)>=start & as.Date(Month)<=today)
m <- unique(hcpTimeTable$Month)
h <- unique(hcpTimeTable$hcp)
g <- as.data.table(expand.grid(h,m))
setnames(g,1:2,c("hcp","Month"))
setkey(g,hcp,Month)
setkey(hcpTimeTable,hcp,Month)
hcpTimeTable <- hcpTimeTable[g]
hcpTimeTable[is.na(TRx)]$TRx <- 0
hcpTimeTable[is.na(NRx)]$NRx <- 0
hcpTimeTable[,TRx:=round(TRx)]
hcpTimeTable$Month <- as.Date(hcpTimeTable$Month)
addToMetaTable("hcpTimeTable","TRx","Metric")
addToMetaTable("hcpTimeTable","NRx","Metric")
addToMetaTable("hcpTimeTable","hcp","Group")
addToMetaTable("hcpTimeTable","Month","Time")

# add event counts from interactionTable to the hcpTimeTable
t <- as.data.table(as.data.frame(xtabs(~hcp+Month+Tactic+Activity,interactionTable)))
t <- t[Freq>0]

s <- as.data.table(dcast(t[,list(hcp,Month,Tactic,Freq)],hcp+Month~Tactic,value.var="Freq",fun.aggregate=sum))
s$Month <- as.Date(s$Month)
hcpTimeTable$Month <- as.Date(hcpTimeTable$Month)
setkeyv(s,c("hcp","Month"))
setkeyv(hcpTimeTable,c("hcp","Month"))
hcpTimeTable <- hcpTimeTable[s]
hcpTimeTable[is.na(hcpTimeTable)] <- 0

s <- as.data.table(dcast(t[,list(hcp,Month,Activity,Freq)],hcp+Month~Activity,value.var="Freq",fun.aggregate=sum))
s$Month <- as.Date(s$Month)
hcpTimeTable$Month <- as.Date(hcpTimeTable$Month)
setkeyv(s,c("hcp","Month"))
setkeyv(hcpTimeTable,c("hcp","Month"))
hcpTimeTable <- hcpTimeTable[s]
hcpTimeTable[is.na(hcpTimeTable)] <- 0

t <- colnames(hcpTimeTable)
for(i in 6:length(t))
    {
        addToMetaTable("hcpTimeTable",t[i],"Group")
    }

# add NP estimates
for(i in unique(hcpTimeTable$hcp))
    {
        hcpTable[hcp==i,c("N","Propensity","Delta_Propensity"):= estimateNP(hcpTimeTable[hcp==i])]
        hcpTable[hcp==i,TRx_slope:=estimateSlope(formula(TRx~Month),hcpTimeTable[hcp==i])]
        hcpTable[hcp==i,RTE_Open_Rate:=estimateRatio(paste("sum(hcpTimeTable[hcp==i]$RTE_Opened)/sum(hcpTimeTable[hcp==i]$Send_RTE)")]
        hcpTable[hcp==i,RTE_Click_Rate:=estimateRatio("sum(hcpTimeTable[hcp==i]$RTE_Clicked)/sum(hcpTimeTable[hcp==i]$RTE_Opened)")]
    }
# add N and P to metaTable field names
addToMetaTable("hcpTable","N","Target")
addToMetaTable("hcpTable","Propensity","Target")
addToMetaTable("hcpTable","Delta_Propensity","Target")
addToMetaTable("hcpTable","Target","Group")
addToMetaTable("hcpTable","TRx_slope","Target")
addToMetaTable("hcpTable","RTE_Open_Rate","Target")
addToMetaTable("hcpTable","RTE_Click_Rate","Target")

# add prob of seeing specific TRx value
hcpTimeTable[,Likelihood:=pbinom(TRx,round(hcpTable[hcp==hcp]$N),hcpTable[hcp==hcp]$Propensity)]
addToMetaTable("hcpTimeTable","Likelihood","Metric")

for(i in hcpTable$hcp)
    {
        t <- hcpTimeTable[hcp==i]$Likelihood
        j <- length(t)
        if(j>1)hcpTable[hcp==i,DP4:=mean(t[(j-1):j])-mean(t)]
        if(j>2)hcpTable[hcp==i,DP3:=mean(t[(j-2):j])-mean(t)]
        if(j>3)hcpTable[hcp==i,DP2:=mean(t[(j-3):j])-mean(t)]
    }
addToMetaTable("hcpTable","DP4","Target")
addToMetaTable("hcpTable","DP3","Target")
addToMetaTable("hcpTable","DP2","Target")

setkey(interactionTable,hcp,Day)
for(i in hcpTable$hcp)
    {
        t <- interactionTable[hcp==i & Activity=="Visit"]
        t[,Diff:=c(0,diff(Day))]
        fit <- estimateLambda(t[Diff>0]$Diff)
#        hcpTable[hcp==i,c("Visit_0","Visit_1"):= list(1/exp(fit[[1]]),1/exp(fit[[2]]))]
        hcpTable[hcp==i,c("Visit_0","Visit_1"):= list(fit[[1]],fit[[2]])]
        hcpTable[hcp==i,AveVisit:= mean(t[Diff>0]$Diff)]
      }
addToMetaTable("hcpTable","Visit_0","Treatment")
addToMetaTable("hcpTable","Visit_1","Treatment")
addToMetaTable("hcpTable","AveVisit","Treatment")


# finally build the groups table
groups <- buildGroups(metaTable)

# and save datasets for use in UI
saveRDS(interactionTable,"./data/interactions_pfizer_example_20150225.RData")
saveRDS(groups,"./data/groups_pfizer_example_20150225.RData")
saveRDS(metaTable,"./data/metaTable_pfizer_example_20150225.RData")
saveRDS(hcpTable,"./data/hcpTable_pfizer_20150225.RData")
saveRDS(hcpTimeTable,"./data/hcpTimeTable_pfizer_20150225.RData")
