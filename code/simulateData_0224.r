library(data.table)
library(NHPoisson)

# functions need to prepare data

addToMetaTable <- function(tab,fld,typ)
    {
        t <- data.table(table=tab,field=fld,type=typ)
        metaTable <<- rbind(metaTable,t,fill=TRUE)
    }

estimateSlope <- function(f,x)
    {
       fit <- lm(f,data=x)
       return(fit$coefficients[[2]])
    }

estimateLambda <- function(events)
    {
       fit <- fitPP.fun(posE=events,covariates=cbind(rep(1:sum(events))),start=list(b0=0,b1=1),modSim=TRUE,dplot=FALSE)
       return(slot(fit,"coef"))
    }

estimateNP <- function(H)
    {
        alpha <-  1   # parameter for n_1 estimate
        lambda <- 1   # parameter for p_1 estimate

        lookback <- 5  # number of months back from end of horizon for P2 calculation

        ###############################################
        # n_1 and p_1 estimate from DasGupta and Rubin
        ###############################################

        s2 <- var(H$TRx)
        Xbar <- mean(H$TRx)
        Xk <- max(H$TRx)
        k <- dim(H)[1]

        n1 <- ((Xk**(alpha+1))*(s2**alpha))/((Xbar**alpha)*((Xk-Xbar)**alpha))

        p1 <- ((Xbar**((alpha+2)*lambda-1))*((Xbar-s2)**(1-lambda))*((Xk-Xbar)**(alpha*lambda)))/((Xk**((alpha+1)*lambda))*(s2**(alpha*lambda)))

        n1i <- trunc(n1)
        n2 <-  Xk
        for(j in 0:(n1i-2)) n2 <- n2 + qbeta(1/k,j+1,n1i-j)

        n <- H$TRx
        p2 <- mean(n[length(n):1][1:lookback])/n1

        return(list(n1,p1,p2))
    }

# setup tables

metaTable <- data.table(table=character(),field=character(),type=character())
interactionTable <- data.table(hcp=factor(),rep=factor(),Day=as.Date(character()),EventType=integer(),diff=numeric())
hcpTimeTable <- data.table(hcp=factor(),Week=as.Date(character()),TRx=integer())

#######################################################
# sample events and TRx
#######################################################

eventNo <- 50
hcpNo <- 40
eventTypeNo <- 2
repNo <- 1
for(hcp in 1:hcpNo)
    {
          rep <- round(repNo*runif(eventNo))
          if(hcp>10 & hcp <=20)     diff <- rexp(eventNo,1/4)
          else if(hcp>20 & hcp<=30) diff <- c(rexp(eventNo/2,1/4),rexp(eventNo/2,1/8))
          else if(hcp>30)           diff <- c(rexp(eventNo/2,1/4),rexp(eventNo/2,1/8))
          else                      diff <- rexp(eventNo,1/4)

          evnt <- c(rep(1,eventNo/2),rep(2,eventNo/2))
          day <- cumsum(diff)
          startday <- Sys.Date()-max(day)
          interactionTable <- rbind(interactionTable,data.table(hcp=hcp,rep=rep,Day=startday+cumsum(diff),EventType=evnt,diff=diff))
     }

weeks <- as.numeric(max(as.Date(cut(interactionTable$Day,"week")))-min(as.Date(cut(interactionTable$Day,"week"))))/7
weeks <- round((weeks/2)+.25)*2   # make sure there are even number of weeks
startday <- min(interactionTable$Day)

patientNo <- 10
for(hcp in 1:hcpNo)
    {
          if(hcp>10 & hcp <=20)        trxs <- c(rbinom(weeks/2,patientNo,.2),rbinom(weeks/2,patientNo,.5))
          else if(hcp>20 & hcp<=30)    trxs <- c(rbinom(weeks,patientNo,.2))
          else if(hcp>30)              trxs <- c(rbinom(weeks/2,patientNo,.2),rbinom(weeks/2,patientNo,.5))
          else                         trxs <- rbinom(weeks,patientNo,.2)
          hcpTimeTable <- rbind(hcpTimeTable,data.table(hcp=hcp,Week=as.Date(startday+(1:weeks)*7),TRx=trxs))
     }
addToMetaTable("hcpTimeTable","hcp","Unit")
addToMetaTable("hcpTimeTable","TRx","Metric")
addToMetaTable("hcpTimeTable","Week","Time")

#######################################################
# end sample events and TRx
#######################################################
#######################################################

# cut up the days in weeks and months
hcpTimeTable[,Week:=cut(Week,"week")]
interactionTable[,Week:=cut(Day,"week")]
interactionTable[,Month:=cut(Day,"month")]

# add info to metatable
addToMetaTable("interactionTable","EventType","Group")
addToMetaTable("interactionTable","hcp","Unit")
addToMetaTable("interactionTable","rep","Group")
addToMetaTable("interactionTable","Day","Time")
addToMetaTable("interactionTable","Week","Time")
addToMetaTable("interactionTable","Month","Time")

# build the hcpTable
hcpTable <- data.table(hcp=factor(1:hcpNo))

for(i in 1:hcpNo)
    {
        hcpTable[hcp==i,c("N","Propensity","Delta_Propensity"):= estimateNP(hcpTimeTable[hcp==i])]
    }

# add N and P to metaTable field names
addToMetaTable("hcpTable","N","Target")
addToMetaTable("hcpTable","Propensity","Target")
addToMetaTable("hcpTable","Delta_Propensity","Target")

# add prob of seeing specific TRx value
hcpTimeTable[,Likelihood:=pbinom(round(TRx),round(hcpTable[hcp]$N),hcpTable[hcp]$Propensity)]
addToMetaTable("hcpTimeTable","Likelihood","Metric")

# add the between time estimates
for(j in 1:eventTypeNo)
    {
        name <- paste("InterEvent_",as.character(j),sep="")
        n1 <- paste(name,"_b_0",sep="")
        n2 <- paste(name,"_b_1",sep="")
        for(i in 1:hcpNo)
            {
                fit <- estimateLambda(interactionTable[hcp==i & EventType==j]$diff)
                hcpTable[hcp==i,c(n1,n2):= list(1/exp(fit[[1]]),1/exp(fit[[2]]))]
            }
        addToMetaTable("hcpTable",n1,"Treatment")
        addToMetaTable("hcpTable",n2,"Treatment")
    }

# add slope of TRx
for(i in 1:hcpNo)
    {
        hcpTable[hcp==i,TRx_slope:=estimateSlope(formula(TRx~Week),hcpTimeTable[hcp==i])]
    }
addToMetaTable("hcpTable","TRx_slope","Target")

# add slope of Likelihood estimates
for(i in 1:hcpNo)
    {
        hcpTable[hcp==i,Likelihood_slope:=estimateSlope(formula(Likelihood~Week),hcpTimeTable[hcp==i])]
    }
addToMetaTable("hcpTable","Likelihood_slope","Target")
setkey(metaTable,table,field,type)

# add event counts from interactionTable to the hcpTimeTable
t <- as.data.table(as.data.frame(xtabs(~hcp+Week+EventType,interactionTable)))
t <- as.data.table(dcast(t,hcp+Week~EventType,value.var="Freq"))
t$Week <- as.Date(t$Week)
setnames(t,c("1","2"),c("EventType_1","EventType_2"))
hcpTimeTable$Week <- as.Date(hcpTimeTable$Week)
setkeyv(t,c("hcp","Week"))
setkeyv(hcpTimeTable,c("hcp","Week"))
hcpTimeTable <- hcpTimeTable[t]
addToMetaTable("hcpTimeTable","EventType_1","Group")
addToMetaTable("hcpTimeTable","EventType_2","Group")

# build table of groups and values for the UI
groups <- data.table(V1=character(),fieldName=character())
setkey(metaTable,field,type)
tbl <- unique(metaTable)
for(i in 1:dim(tbl)[1])
    {
        table <- tbl[i]$table; field <- tbl[i]$field; type <- tbl[i]$type
        if(type=="Group" | type=="Unit" )
            {
                var <- paste(table,"$",field,sep="")
                strg <- paste(var,"<-as.factor(",var,")",sep="") # make sure all groups are factors
                eval(parse(text=strg))
                strg <- paste("levels(",var,")",sep="") # get all the levels
                t <- as.data.table(eval(parse(text=strg)))
                t$fieldName <- field
                groups <- rbind(groups,t)
            }
    }

setnames(groups,"V1","valueName")


saveRDS(interactionTable,"./data/interactions_reference_20150223.RData")
saveRDS(groups,"./data/groups_reference_20150223.RData")
saveRDS(metaTable,"./data/metaTable_reference_20150223.RData")
saveRDS(hcpTable,"./data/hcpTable_reference_20150223.RData")
saveRDS(hcpTimeTable,"./data/hcpTimeTable_reference_20150223.RData")
