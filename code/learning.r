###
#  this is a test file for experimenting with analytical approaches
###

i <- 30;
t <- interactionTable[hcp==i & Activity=="Visit"];
t[,Diff:=c(0,diff(Day))];
t$ctr <- 1
events <- t[Diff>0,list(Diff,ctr)]
events[,tot:=sum(ctr),by=Diff]

cov <- c();
for(i in 1:length(events$Diff)){ cov <- c(cov,1:events$Diff[i]) };
cov <- cbind(cov,1:length(cov))

fit <- fitPP.fun(posE=events$Diff,covariates=cov,start=list(b0=0,b1=1,b2=1),modSim=TRUE,dplot=TRUE)
result <- slot(fit,"coef")

v <- data.table(day=rep(0,max(events$Diff)))
v[events$Diff] <- events$tot
plot(v$day,type="h")

ResDE <- CalcResD.fun(mlePP=fit,lint=20)


i <- 13;
t <- interactionTable[hcp==i & Activity=="Visit"];
t[,Diff:=c(0,diff(Day))];
t$ctr <- 1
events <- t[Diff>0,list(Diff,ctr)]
events[,tot:=sum(ctr),by=Diff]

cov <- data.table(day=rep(0,max(events)))
cov[events$Diff] <- events$tot

plot(cov$day,type="h")
plot.frequency.spectrum(fft(cov$day))

plot.frequency.spectrum <- function(X.k, xlimits=c(0,length(X.k))) {
  plot.data  <- cbind(0:(length(X.k)-1), Mod(X.k))

  # TODO: why this scaling is necessary?
  plot.data[2:length(X.k),2] <- 2*plot.data[2:length(X.k),2]

  plot(plot.data, t="h", lwd=2, main="",
       xlab="Frequency (Hz)", ylab="Strength",
       xlim=xlimits, ylim=c(0,max(Mod(plot.data[,2]))))
}
