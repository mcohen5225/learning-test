library(data.table)
library(reshape2)

source("stats.r")
source("utils.r")

# setup tables

metaTable <- data.table(table=character(),field=character(),type=character())
interactionTable <- data.table(hcp=factor(),rep=factor(),Day=as.Date(character()),EventType=integer(),Diff=numeric())
hcpTimeTable <- data.table(hcp=factor(),Week=as.Date(character()),TRx=integer())

#######################################################
# sample events and TRx
#######################################################

eventNo <- 50
hcpNo <- 400
eventTypeNo <- 2
repNo <- 1
for(hcp in 1:hcpNo)
    {
          rep <- round(repNo*runif(eventNo))
          if(hcp<=hcpNo/4)
              {
                  diff <- c(rexp(eventNo/2,1/16),rexp(eventNo/2,1/4))
                  evnt <- rep(1,eventNo)
              }
          else if(hcp<=2*hcpNo/4)
              {
                  diff <- rexp(eventNo,1/8)
                  evnt <- c(rep(1,eventNo/2),rep(2,eventNo/2))
              }
          else if(hcp<=3*hcpNo/4)
              {
                  diff <- rexp(eventNo,1/8)
                  evnt <- rep(1,eventNo)
              }
          else
              {
                  diff <- rexp(eventNo,1/8)
                  evnt <- rep(2,eventNo)
              }
          day <- cumsum(diff)
          startday <- Sys.Date()-max(day)
          interactionTable <- rbind(interactionTable,data.table(hcp=hcp,rep=rep,Day=startday+cumsum(diff),EventType=evnt,Diff=diff))
     }

weeks <- as.numeric(max(as.Date(cut(interactionTable$Day,"week")))-min(as.Date(cut(interactionTable$Day,"week"))))/7
weeks <- round((weeks/2)+.25)*2   # make sure there are even number of weeks
startday <- min(interactionTable$Day)

patientNo <- 10
lowP <- .45
highP <- .55
for(hcp in 1:hcpNo)
    {
          if(hcp<=hcpNo/4)          trxs <- c(rbinom(weeks/2,patientNo,lowP),rbinom(weeks/2,patientNo,highP))
          else if(hcp<=2*hcpNo/4)   trxs <- c(rbinom(weeks/2,patientNo,lowP),rbinom(weeks/2,patientNo,highP))
          else if(hcp<=3*hcpNo/4)   trxs <- c(rbinom(weeks/2,patientNo,highP),rbinom(weeks/2,patientNo,lowP))
          else                      trxs <- c(rbinom(weeks/2,patientNo,highP),rbinom(weeks/2,patientNo,lowP))
          hcpTimeTable <- rbind(hcpTimeTable,data.table(hcp=hcp,Week=as.Date(startday+(1:weeks)*7),TRx=trxs))
     }
addToMetaTable("hcpTimeTable","hcp","Group")
addToMetaTable("hcpTimeTable","TRx","Metric")
addToMetaTable("hcpTimeTable","Week","Time")


#######################################################
# end sample events and TRx
#######################################################
#######################################################

# cut up the days in weeks and months
hcpTimeTable[,Week:=cut(Week,"week")]
interactionTable[,Week:=cut(Day,"week")]
interactionTable[,Month:=cut(Day,"month")]

# add info to metatable
addToMetaTable("interactionTable","EventType","Group")
addToMetaTable("interactionTable","hcp","Group")
addToMetaTable("interactionTable","rep","Group")
addToMetaTable("interactionTable","Day","Time")
addToMetaTable("interactionTable","Week","Time")
addToMetaTable("interactionTable","Month","Time")
addToMetaTable("interactionTable","Diff","Metric")

# build the hcpTable
hcpTable <- data.table(hcp=factor(1:hcpNo))

for(i in 1:hcpNo)
    {
        hcpTable[hcp==i,c("N","Propensity","Delta_Propensity"):= estimateNP(hcpTimeTable[hcp==i])]
    }

# add N and P to metaTable field names
addToMetaTable("hcpTable","N","Target")
addToMetaTable("hcpTable","Propensity","Target")
addToMetaTable("hcpTable","Delta_Propensity","Target")

# add prob of seeing specific TRx value
hcpTimeTable[,Likelihood:=pbinom(round(TRx),round(hcpTable[hcp]$N),hcpTable[hcp]$Propensity)]
addToMetaTable("hcpTimeTable","Likelihood","Metric")

# add the between time estimates
for(j in 1:eventTypeNo)
    {
        name <- paste("InterEvent_",as.character(j),sep="")
        n1 <- paste(name,"_b_0",sep="")
        n2 <- paste(name,"_b_1",sep="")
        for(i in 1:hcpNo)
            {
                fit <- estimateLambda(interactionTable[hcp==i & EventType==j]$Diff)
                hcpTable[hcp==i,c(n1,n2):= list(fit[[1]],fit[[2]])]
            }
        addToMetaTable("hcpTable",n1,"Treatment")
        addToMetaTable("hcpTable",n2,"Treatment")
    }

# add slope of TRx
for(i in 1:hcpNo)
    {
        hcpTable[hcp==i,TRx_slope:=estimateSlope(formula(TRx~Week),hcpTimeTable[hcp==i])]
    }
addToMetaTable("hcpTable","TRx_slope","Target")

# add slope of Likelihood estimates
for(i in 1:hcpNo)
    {
        hcpTable[hcp==i,Likelihood_slope:=estimateSlope(formula(Likelihood~Week),hcpTimeTable[hcp==i])]
    }
addToMetaTable("hcpTable","Likelihood_slope","Target")
setkey(metaTable,table,field,type)

# add event counts from interactionTable to the hcpTimeTable
t <- as.data.table(as.data.frame(xtabs(~hcp+Week+EventType,interactionTable)))
t <- t[Freq>0]
t[,EventType:=paste("EventType_",as.character(EventType),sep="")]
t <- as.data.table(dcast(t,hcp+Week~EventType,value.var="Freq",fun.aggregate=sum))
t$Week <- as.Date(t$Week)
hcpTimeTable$Week <- as.Date(hcpTimeTable$Week)
setkeyv(t,c("hcp","Week"))
setkeyv(hcpTimeTable,c("hcp","Week"))
hcpTimeTable <- hcpTimeTable[t]
hcpTimeTable[is.na(hcpTimeTable)] <- 0
addToMetaTable("hcpTimeTable","EventType_1","Group")
addToMetaTable("hcpTimeTable","EventType_2","Group")

#######################
# add groupping of hcp
#######################
hcpTable$Attribute <- as.factor("D")
hcpTable[as.numeric(hcp)<=3*hcpNo/4,Attribute:=as.factor("C")]
hcpTable[as.numeric(hcp)<=2*hcpNo/4,Attribute:=as.factor("B")]
hcpTable[as.numeric(hcp)<=hcpNo/4,Attribute:=as.factor("A")]
addToMetaTable("hcpTable","Attribute","Group")

hcpTimeTable$Attribute <- as.factor("D")
hcpTimeTable[as.numeric(hcp)<=3*hcpNo/4,Attribute:=as.factor("C")]
hcpTimeTable[as.numeric(hcp)<=2*hcpNo/4,Attribute:=as.factor("B")]
hcpTimeTable[as.numeric(hcp)<=hcpNo/4,Attribute:=as.factor("A")]
addToMetaTable("hcpTimeTable","Attribute","Group")

interactionTable$Attribute <- as.factor("D")
interactionTable[as.numeric(hcp)<=3*hcpNo/4,Attribute:=as.factor("C")]
interactionTable[as.numeric(hcp)<=2*hcpNo/4,Attribute:=as.factor("B")]
interactionTable[as.numeric(hcp)<=hcpNo/4,Attribute:=as.factor("A")]
addToMetaTable("interactionTable","Attribute","Group")

for(i in hcpTable$hcp)
    {
        t <- hcpTimeTable[hcp==i]$Likelihood
        j <- length(t)
        if(j>1)hcpTable[hcp==i,DP4:=mean(t[(j-1):j])-mean(t)]
        if(j>2)hcpTable[hcp==i,DP3:=mean(t[(j-2):j])-mean(t)]
        if(j>3)hcpTable[hcp==i,DP2:=mean(t[(j-3):j])-mean(t)]
    }
addToMetaTable("hcpTable","DP4","Target")
addToMetaTable("hcpTable","DP3","Target")
addToMetaTable("hcpTable","DP2","Target")

groups <- buildGroups(metaTable)

saveRDS(interactionTable,"./data/interactions_reference_20150223.RData")
saveRDS(groups,"./data/groups_reference_20150223.RData")
saveRDS(metaTable,"./data/metaTable_reference_20150223.RData")
saveRDS(hcpTable,"./data/hcpTable_reference_20150223.RData")
saveRDS(hcpTimeTable,"./data/hcpTimeTable_reference_20150223.RData")
