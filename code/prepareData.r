library(data.table)
library(reshape2)

# functions need to prepare data

source("stats.r")
source("utils.r")

# read data

metaTable <- data.table(table=character(),field=character(),type=character())
hcpTimeTable <- readRDS("./data/hcpTableTime_20141230.RData")
hcpTable <- readRDS("./data/hcpTable_20150213.RData")
repTable <- readRDS("./data/repTable_20141218.RData")
interactionTable <- readRDS("./data/interactionTable_20141218.RData")
tacticTable <- readRDS("./data/tacticTable_20141218.RData")


############
# setup dates
############
today <- Sys.Date()
start <- as.Date("2014-05-01")

######## experimental section
interactionTable[is.na(Tactic)]$Tactic <- "None"    # may want to change this
interactionTable[,Week:=cut(date,"week")]
interactionTable[,Month:=cut(date,"month")]
setnames(interactionTable,"date","Day")


setkey(repTable,"rep")
setkey(interactionTable,"rep")
interactionTable <- repTable[interactionTable]

setkey(interactionTable,"Tactic")
setkey(tacticTable,"Tactic")
interactionTable <- tacticTable[interactionTable]

hcpTable[,c("N","P","P2"):=NULL]  # cleanup

# restrict time interval
interactionTable <- subset(interactionTable,Day>=start & Day <= today)

# add to metaTable
addToMetaTable("interactionTable","Tactic","Group")
addToMetaTable("interactionTable","Message","Group")
addToMetaTable("interactionTable","Name","Group")
addToMetaTable("interactionTable","District","Group")
addToMetaTable("interactionTable","Role","Group")
addToMetaTable("interactionTable","hcp","Group")
addToMetaTable("interactionTable","Activity","Group")
addToMetaTable("interactionTable","Month","Time")
addToMetaTable("interactionTable","Day","Time")
addToMetaTable("interactionTable","Week","Time")

# process hcpTimeTable
hcpTimeTable <- subset(hcpTimeTable,as.Date(Month)>=start & as.Date(Month)<=today)
m <- unique(hcpTimeTable$Month)
h <- unique(hcpTimeTable$hcp)
g <- as.data.table(expand.grid(h,m))
setnames(g,1:2,c("hcp","Month"))
setkey(g,hcp,Month)
setkey(hcpTimeTable,hcp,Month)
hcpTimeTable <- hcpTimeTable[g]
hcpTimeTable[is.na(TRx)]$TRx <- 0
hcpTimeTable[is.na(NRx)]$NRx <- 0
hcpTimeTable[,TRx:=round(TRx)]
hcpTimeTable$Month <- as.Date(hcpTimeTable$Month)
addToMetaTable("hcpTimeTable","TRx","Metric")
addToMetaTable("hcpTimeTable","NRx","Metric")
addToMetaTable("hcpTimeTable","hcp","Group")
addToMetaTable("hcpTimeTable","Month","Time")


# add NP estimates
for(i in unique(hcpTimeTable$hcp))
    {
        hcpTable[hcp==i,c("N","Propensity","Delta_Propensity"):= estimateNP(hcpTimeTable[hcp==i])]
    }
# add N and P to metaTable field names
addToMetaTable("hcpTable","N","Target")
addToMetaTable("hcpTable","Propensity","Target")
addToMetaTable("hcpTable","Delta_Propensity","Target")
addToMetaTable("hcpTable","Target","Group")

# add prob of seeing specific TRx value
hcpTimeTable[,Likelihood:=pbinom(TRx,round(hcpTable[hcp==hcp]$N),hcpTable[hcp==hcp]$Propensity)]
addToMetaTable("hcpTimeTable","Likelihood","Metric")

# add event counts from interactionTable to the hcpTimeTable
t <- as.data.table(as.data.frame(xtabs(~hcp+Month+Tactic+Activity+District,interactionTable)))
t <- as.data.table(dcast(t,hcp+Month~Tactic+Activity+District,value.var="Freq"))
t$Month <- as.Date(t$Month)
hcpTimeTable$Month <- as.Date(hcpTimeTable$Month)
setkeyv(t,c("hcp","Month"))
setkeyv(hcpTimeTable,c("hcp","Month"))
hcpTimeTable <- hcpTimeTable[t]
hcpTimeTable[is.na(hcpTimeTable)] <- 0
t <- colnames(hcpTimeTable)
for(i in 6:length(t))
    {
        addToMetaTable("hcpTimeTable",t[i],"Group")
    }

groups <- buildGroups(metaTable)

saveRDS(interactionTable,"./data/interactions_pfizer_example_20150225.RData")
saveRDS(groups,"./data/groups_pfizer_example_20150225.RData")
saveRDS(metaTable,"./data/metaTable_pfizer_example_20150225.RData")
saveRDS(hcpTable,"./data/hcpTable_pfizer_20150225.RData")
saveRDS(hcpTimeTable,"./data/hcpTimeTable_pfizer_20150225.RData")
