library(NHPoisson)

estimateRatio <- function(f)
    {
        ratio <- eval(parse(text=f))
        if(is.na(ratio))ratio <- 0
        return(ratio)
    }

estimateSlope <- function(f,x)
    {
       fit <- lm(f,data=x)
       return(fit$coefficients[[2]])
    }

estimateLambda <- function(events)
    {
        if(length(events)>2)
            {
             cov <- c()
             for(i in 1:length(events)){ cov <- c(cov,1:events[i]) }
             fit <- fitPP.fun(posE=events,covariates=cbind(cov),start=list(b0=0,b1=1),modSim=TRUE,dplot=FALSE)
             return(slot(fit,"coef"))
            }
        return(list(NA,NA))
    }

estimateNP <- function(H)
    {
        alpha <-  1   # parameter for n_1 estimate
        lambda <- 1   # parameter for p_1 estimate

        lookback <- 5  # number of months back from end of horizon for P2 calculation

        ###############################################
        # n_1 and p_1 estimate from DasGupta and Rubin
        ###############################################

        s2 <- var(H$TRx)
        Xbar <- mean(H$TRx)
        Xk <- max(H$TRx)
        k <- dim(H)[1]

        if(k==0)return(list(0,0,0))
        if(k==1)return(list(Xbar,1,0))
        if(s2==0)return(list(Xbar,1,0))

        n1 <- ((Xk**(alpha+1))*(s2**alpha))/((Xbar**alpha)*((Xk-Xbar)**alpha))

        p1 <- ((Xbar**((alpha+2)*lambda-1))*((Xbar-s2)**(1-lambda))*((Xk-Xbar)**(alpha*lambda)))/((Xk**((alpha+1)*lambda))*(s2**(alpha*lambda)))
        n1i <- trunc(n1)
#        n2 <-  Xk

#        for(j in 0:(n1i-2)) n2 <- n2 + qbeta(1/k,j+1,n1i-j)

        n <- H$TRx
        p2 <- mean(n[length(n):1][1:lookback])/n1

        return(list(n1,p1,p2-p1))
    }


