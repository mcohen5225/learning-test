library(data.table)
library(lattice)
library(psych)

if(exists("referenceDB"))
    {
     interactionTable <- readRDS("./data/interactions_reference_20150223.RData")
     groups <- readRDS("./data/groups_reference_20150223.RData")
     metaTable <- readRDS("./data/metaTable_reference_20150223.RData")
     hcpTable <- readRDS("./data/hcpTable_reference_20150223.RData")
     hcpTimeTable <- readRDS("./data/hcpTimeTable_reference_20150223.RData")
 } else if(exists("pfizerDB"))
    {
     interactionTable <- readRDS("./data/interactions_pfizer_example_20150225.RData")
     groups <- readRDS("./data/groups_pfizer_example_20150225.RData")
     metaTable <- readRDS("./data/metaTable_pfizer_example_20150225.RData")
     hcpTable <- readRDS("./data/hcpTable_pfizer_20150225.RData")
     hcpTimeTable <- readRDS("./data/hcpTimeTable_pfizer_20150225.RData")
 } else if(exists("lillyDB"))
    {
     interactionTable <- readRDS("../Lilly/data/eds_interactions.RData")
     groups <- readRDS("../Lilly/data/eds_groups.RData")
     metaTable <- readRDS("../Lilly/data/eds_metaTable.RData")
     hcpTable <- readRDS("../Lilly/data/eds_hcpTable.RData")
     hcpTimeTable <- readRDS("../Lilly/data/eds_hcpTimeTable.RData")
 }


# these three statements are for the UI
selectionNamesC <- metaTable[table=="interactionTable" & (type=="Group" | type=="Unit")]$field
selectionNamesM <- metaTable[table=="hcpTimeTable" & (type=="Group" | type=="Unit")]$field
groups$selectC <- -1
groups[fieldName %in% metaTable[table=="interactionTable"]$field]$selectC <- 1
groups$selectM <- -1
groups[fieldName %in% metaTable[table=="hcpTimeTable"]$field]$selectM <- 1
groups$selectS <- -1
groups[fieldName %in% metaTable[table=="hcpTable"]$field]$selectS <- 1

dimensions <- function(tabType,fldType)
    {
        if(fldType==".")return(metaTable[table==tabType]$field)
        else if(tabType==".")return(metaTable[type==fldType]$field)
        return(metaTable[table==tabType & type==fldType]$field)
    }


############
# setup dates for UI
############
start <- min(interactionTable$Day)
end <- max(interactionTable$Day)

############
# graphics parameters
############
supsym <- trellis.par.get("superpose.symbol")
    supsym$col <- c("red","blue","green","brown","black","yellow")
    supsym$fill <- c("red","blue","green","brown","black","yellow")  # fill colors same as outline colors
    supsym$pch <- c(17,18,15,20,21,22)
    supsym$cex <- c(.7,.7,.5) # otherwise symbol 15 appears a bit larger
